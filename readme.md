# Musica-api

## Requirements
Ensure to have these modules on your environment before do setup.
* **node v10.15.13+**
* **npm v6.13+**

## Setup
1. Clone this repo or do manual pull from your local repository.
2. Install all dependencies by running the command below
```bash
npm i
```
3. Change a file with a name **localenv** to **.env** to create your own global variables and set value as you will. For instance:
```
// This is a content example from .env

PORT=4001
```

## Usage
Simply just run this command
```
npm start
```
You **don't need to restart** the app everytime if you done to change some files, **nodemon** will do work for you to restart the app automatically everytime you changed something.

## Known bugs
1. 