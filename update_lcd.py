
#!/usr/bin/python
from Adafruit_CharLCD import Adafruit_CharLCD
from time import sleep
import sys

#  Initialize LCD (must specify pinout and dimensions)
lcd = Adafruit_CharLCD(rs=26, en=19,
                       d4=13, d5=6, d6=5, d7=11,
                       cols=16, lines=2)

msg = sys.argv[1]
if len(msg) > 16:
	msg = msg[0:16]
	msg = msg.split("(")[0]

lcd.clear()
lcd.set_cursor(0, 0)
lcd.message("192.168.4.1:5000")
lcd.set_cursor(0, 1)
lcd.message(msg)
