
// Get own library with src directory as root
global.reqlib = (path) => {
    return require(require('app-root-path') + "/server/" + path);
}

global.assetsPath = require('app-root-path') + "/assets";

// Get global environment variables
require('dotenv').config();

const debug = Boolean(process.env.DEBUG) || true;

// Modules
const ip = require('ip');
const express = require('express')
const http = require('http')
const socketIO = require('socket.io')
const MPlayer = require('mplayer')
const player = new MPlayer()
const shell = require('shelljs')
const Gpio = require('onoff').Gpio;
const gpioList = [new Gpio(21, 'out'), new Gpio(20, 'out'), new Gpio(16, 'out'), new Gpio(12, 'out'), new Gpio(7, 'out')];

player.on('error', err => debug && console.error('player error:', err));

const app = express();
const server = http.createServer(app);
const io = socketIO(server);
io.on('error', err => debug && console.error('io error:', err));
const ytdl = reqlib("services/youtube-dl");

let queue = [];
let counter = 0;
let currentlyPlaying = false;
let canceledCurrentDownload = false;
let currentlyPaused = false;

const progress = (current, total) => {
    let percentage =  Math.floor((current/total) * 5);
    gpioList.forEach((pin) => {
	pin.writeSync(0);
    });

   for (let i=0; i< percentage;i++) {
	gpioList[i].writeSync(1);
	console.log("on" + i);
  }
}

const reset = () => {
  gpioList.forEach((pin) => {
        pin.writeSync(0);
    });
}

const updateLCD = async(title) => {
  shell.exec(`python3 /home/pi/server/api_main/update_lcd.py "${title}"`)
}

const play = () => {
    if (debug) console.log('queue:', queue)
    if (queue.length === 0) return;
    let stream;
    const song = queue[0]

    if (!song.isDownloaded) {
	reset();
        io.sockets.emit('downloading', {status: true});
        if (debug) console.log(`Downloading ${song.title}`)
        stream = ytdl.getMP3(song.id);
        canceledCurrentDownload = false;
        stream.on('error', err => console.error('stream error:', err));
        stream.on('progress', (chunkLength, downloaded, contentLength) => {
            if (debug) console.log('downloading', downloaded);
	    progress(downloaded, contentLength);
            if (canceledCurrentDownload) {
                if (debug) console.log('cancel download song');
                io.sockets.emit('downloading', {status: false});
                stream.destroy();
		player.openFile('test.mp3');
		player.stop();
            }
            if (downloaded === contentLength && !canceledCurrentDownload) {
                io.sockets.emit('downloading', {status: false});
                if (debug) console.log(`Downloaded. Playing ${song.title}`)
                song.isDownloaded = true;
                player.openFile(`${song.id}.mp3`)
		updateLCD(song.title)
            }
        })
    } else {
        if (!currentlyPlaying) {
            if (currentlyPaused) {
                if (debug) console.log(`Resuming song ${song.title}`)
                player.play();
            } else {
                if (debug) console.log(`Song already downloaded. Playing ${song.title}`)
                player.openFile(`${song.id}.mp3`)
		updateLCD(song.title)
            }
        }
    }
}

player.on('stop', () => {
    currentlyPlaying = false;
    queue.shift();
    if (queue.length!==0) {
        play(); currentlyPlaying = true;
    }
    io.sockets.emit('currentQueue', {queue, currentlyPlaying})
})

io.on('connection', socket => {
    socket.on('error', err => debug && console.error('socket error:', err));
    console.log(`${socket.handshake.address} [Connected]`);

    socket.on('addToQueue', (song) => {
        queue.some((elem) => {
            return elem.title === song.title;
        }) ?
        queue.push({
            ...song,
            queueId: counter++,
            isDownloaded: true,
        }) :
        queue.push({
            ...song,
            queueId: counter++,
            isDownloaded: false,
        });
        io.sockets.emit('currentQueue', {queue, currentlyPlaying})
        if (debug) console.log(queue);
    });

    socket.on('removeQueue', qid => {
        queue = queue.filter(s => s.queueId !== qid)
        io.sockets.emit('currentQueue', {queue, currentlyPlaying})
        if (debug) console.log(queue);
    });

    socket.on('currentQueue', () => {
        io.sockets.emit('currentQueue', {queue, currentlyPlaying})
    });

    socket.on('play', () => {
        console.log("play")
        if (queue.length!==0) play();
        currentlyPlaying = true; currentlyPaused = false;
        io.sockets.emit('currentQueue', {queue, currentlyPlaying})
    })

    socket.on('pause', () => {
        if (debug) console.log("Paused")
        // const passedQueue = queue.filter((s,idx) => idx!==0);
        // console.log(passedQueue)
        player.pause();
        currentlyPlaying = false; currentlyPaused = true;
        io.sockets.emit('currentQueue', {queue, currentlyPlaying})
    });

    socket.on('skip', () => {
        if (debug) console.log("Skipped current song")
        canceledCurrentDownload = true;
        // queue.shift();
	player.stop();
    });

    socket.on('disconnect', () => {
        if (debug) console.log(`${socket.handshake.address} [Disconnected]`);
    });
})

app.get('/', function(req, res){
    res.sendFile(assetsPath + "/templates/home/index.html");
});

server.listen(process.env.PORT || 4001, () => {
    if (debug) console.log(`Server is built on [${ip.address()}:${process.env.PORT}]`)
});
