const fs = require('fs');
const ytdl = require('ytdl-core');
const touch = (fileName) => fs.createWriteStream(fileName + '.mp3');

exports.getMP3 = (videoId) => {
	const stream = ytdl(
		videoId, {
			quality: 'lowestaudio',
			format: 'mp3',
			filter: 'audioonly',
		})
		stream.pipe(touch(videoId));
		
	return stream
}